# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

STATIC_PATHS = ['images']
AUTHOR = u'saran kumar'
SITENAME = u'saran kumar'
SITEURL = ''
THEME = 'pelican-clean-blog'
DEFAULT_HEADER_IMAGE = "https://SaranRaj.gitlab.com/blog/themes/banner-bg.jpg"
ARCHIVE_HEADER_IMAGE = "https://SaranRaj.gitlab.com/blog/themes/banner-bg.jpg"
AUTHOR_PIC_URL = "http://SaranRaj.gitlab.com/blog/images/profile.jpg"
AUTHOR_BIO = "Developer"
AUTHOR_LOCATION = "Vellore"


PATH = 'content'

TIMEZONE = 'Asia/Kolkata'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Github', 'https://github.com/SaranRaj48'),
         ('Gitlab', 'http://gitlab.com/SaranRaj/'),
         ('Twitter', 'http://twitter.com/SaranRamesh121/'),
         ('@saran:matrix.org', '#'),)

# Social widget
SOCIAL = (('Facebook', 'https://www.facebook.com/Saran Kumar'),
          ('Twitter', 'https://twitter.com/SaranRamesh121'),
	  ('@saran:matrix.org', '#'))

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

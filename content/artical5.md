Title:Summer Camp 2k16
Date:2015-04-25
Category:Post

### Annual Workshop 2k16

Annual Workshop was conducted by the Free Software Foundation Tamil Nadu. The workshop was conducted as a two different parallel tracks. 
 _**Designing track**_ and _**Programing track**_

![](https://saranraj25.files.wordpress.com/2016/09/fb_img_14701238697501.jpg)

### Group photo with all participants & volunteers

![](https://manimaran96.files.wordpress.com/2016/08/summer-camp-2k16-chennai-manimaran96-23.jpg?w=645&h=484)

### Discussion in BEFI Hall

![](https://manimaran96.files.wordpress.com/2016/08/summer-camp-2k16-chennai-manimaran96-25.jpg?w=627&h=353)

###Volunteer from Villupuram GLUG and UCEV GLUG

![](https://manimaran96.files.wordpress.com/2016/08/summer-camp-2k16-chennai-manimaran96-26.jpg?w=638&h=478)




	     __**Programming track**__
	
	* Day-1:

		1. Free Software Phillosophy		     	
		2. GNU/Linux Installation		     
		3. Shell Scripting and Git		    
	
	* Day-2:
		
		1. Hands on Python 		              
		2. Short talk on Licences		     
											
	* Day-3:	
																		
		1. Crypto Party	     			    
		2. Intro to Mesh Network		     
		3. Talk on privacy and Surveillance	     

	      __**Designing track**__

	* Day-1:

		1. Free Software Phillosophy
		2. GNU/Linux Installation
		3. 2D Painting using GIMP

	* Day-2:

		1. 3D Modelling and Animation using Blender
		2. Hands on Blender
		3. Short talk on Licences

	* Day-3:

		1. Vector graphics using Inkscape
		2. Mesh Network	
		3. Talk on privacy and Surveillance



												
		 									
											
		

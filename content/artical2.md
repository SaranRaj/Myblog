Title:Free Software Philosophy
Date:2017-02-06
Category:Technical

_**Free Software**_

The free software definition presents the criteria for whether a particular software program qualifies as free software. From time to time we revise this definition, to clarify it or resolve questions about subtle issues. See the History section below for a list of changes that affect the definition of free software.

_**“Free Software”**_ means software that represents user’s freedom and community. Roughly, it means that the user have the freedom to run, copy, distribute, study, change and improve the software. Thus, _**“free”**_ as in _**“free speech,”**_ not as in _**“free beer”**_. We sometimes call it _**“lib re software,”**_ borrowing the French or Spanish word for _**“free”**_ as in freedom, to show we do not mean the software is gratis.

We campaign for freedom because everyone deserves them. With these freedoms, the user (both individually and collectively) control the program and what it does for them. When users don’t control the program, we call it a “non-free” or “proprietary” program. The non-free program controls the users, and the developer controls the program, this makes the program an instrument of unjust power.

> _A program is free software if the program’s users have the four essentials freedoms:_

_**Freedom 0**_ : The freedom to run the program as you wish, for any purpose.

_**Freedom 1**_ : The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.

_**Freedom 2**_ : The freedom to redistribute copies so you can help your neighbor.

_**Freedom 3**_ : The freedom to distribute copies of your modified various to others. By doing this you can give whole community a change to benefit from your changes. Access to the source code is a precondition for this.

A program is free software if it gives users adequately all of these freedoms. Otherwise, it is nonfree. While we can distinguish various non-free distribution schemes in terms of how far they fall short of begin free, we consider them all equally unethical.
